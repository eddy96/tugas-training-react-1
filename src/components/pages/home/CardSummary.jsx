import React from "react";
import "./css/card-summary.css";

const CardSummary = props => (
  <div
    className="card-summary-block"
    style={{ backgroundColor: props.card.bgColor }}
  >
    <div className="card-summary-content">
      <div className="card-summary-value">
        <h2>{props.card.value}</h2>
        <h6>{props.card.name}</h6>
      </div>
      <div className="card-summary-icon">
        <i className={props.card.logo} aria-hidden="true" />
      </div>
    </div>
    <div className="card-summary-footer">
      <h6>More Info</h6>
      <i class="fa fa-arrow-circle-o-right" aria-hidden="true" />
    </div>
  </div>
);

export default CardSummary;
