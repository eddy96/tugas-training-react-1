import React, { Component } from "react";

class CalculatorComponent extends Component {
  render() {
    const { operator } = this.props;
    const CALCULATOR_MAPPER = {
      "+": "success",
      "-": "danger",
      "*": "info",
      "/": "warning"
    };
    const { onOperatorChange } = this.props;
    return (
      <button
        onClick={() => onOperatorChange(operator)}
        className={"btn btn-" + CALCULATOR_MAPPER[operator]}
      >
        {operator}
      </button>
    );
  }
}

export default CalculatorComponent;
