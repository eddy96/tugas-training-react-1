import React, { Component } from "react";
import CardSummary from "./CardSummary";

class CardSummaryContainer extends Component {
  state = {
    summary: [
      {
        name: "New Orders",
        value: 150,
        logo: "fa fa-shopping-bag",
        bgColor: "rgba(0, 192, 239, 1)"
      },
      {
        name: "Bounce Rate",
        value: 53 + "%",
        logo: "fa fa-bar-chart",
        bgColor: "rgba(0, 166, 90, 1)"
      },
      {
        name: "User Registrations",
        value: 44,
        logo: "fa fa-user",
        bgColor: "rgba(243, 156, 18, 1)"
      },
      {
        name: "Unique Visitors",
        value: 65,
        logo: "fa fa-pie-chart",
        bgColor: "rgba(221, 75, 57, 1)"
      }
    ]
  };

  renderCardSummary = summary => {
    return summary.map(card => <CardSummary card={card} />);
  };

  render() {
    const { summary } = this.state;
    return (
      <div className="container-card">{this.renderCardSummary(summary)}</div>
    );
  }
}

export default CardSummaryContainer;
