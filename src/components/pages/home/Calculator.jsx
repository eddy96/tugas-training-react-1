import React, { Component } from "react";
import CalculatorComponentSummary from "./CalculatorComponentSummary";

class Calculator extends Component {
  state = {
    number: {
      firstNumber: 0,
      secondNumber: 0
    },
    operator: "+",
    operators: ["+", "-", "*", "/"]
  };

  handleChange = event => {
    const { value, name } = event.target;
    let { number } = this.state;
    number[name] = parseInt(value);
    this.setState({ number });
  };

  handleOperatorChange = operator => {
    // let { operator } = this.state;
    this.setState({ operator });
  };

  getResult = (firstNumber, operator, secondNumber) => {
    switch (operator) {
      case "+":
        return firstNumber + secondNumber;
      case "-":
        return firstNumber - secondNumber;
      case "*":
        return firstNumber * secondNumber;
      default:
        return firstNumber / secondNumber;
    }
  };

  render() {
    const { firstNumber, secondNumber } = this.state.number;
    const { operator } = this.state;
    const { handleOperatorChange } = this;

    return (
      <div>
        <h1>Calculator Sederhana</h1>
        <div>
          <input
            name="firstNumber"
            onChange={this.handleChange}
            defaultValue={firstNumber}
          />
          {operator}
          <input
            name="secondNumber"
            onChange={this.handleChange}
            defaultValue={secondNumber}
          />
          <span>=</span>
          <input
            value={this.getResult(firstNumber, operator, secondNumber)}
            disabled
          />
        </div>
        <CalculatorComponentSummary onOperatorChange={handleOperatorChange} />
        {/* <button
          onClick={e => this.handleOperatorChange(e)}
          className="btn btn-success"
          value="+"
        >
          +
        </button>
        <button
          onClick={a => this.handleOperatorChange(a)}
          value="-"
          className="btn btn-warning"
        >
          -
        </button>
        <button
          onClick={b => this.handleOperatorChange(b)}
          value="*"
          className="btn btn-danger"
        >
          *
        </button>
        <button
          onClick={c => this.handleOperatorChange(c)}
          value="/"
          className="btn btn-info"
        >
          /
        </button> */}
      </div>
    );
  }
}

export default Calculator;
