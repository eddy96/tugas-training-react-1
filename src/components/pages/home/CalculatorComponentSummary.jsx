import React, { Component } from "react";
import CalculatorComponent from "./CalculatorComponent";

class CalculatorComponentSummary extends Component {
  state = {
    operators: ["+", "-", "*", "/"]
  };

  renderCalculatorSummary = operators => {
    const { onOperatorChange } = this.props;

    return operators.map(operator => (
      <CalculatorComponent
        onOperatorChange={onOperatorChange}
        operator={operator}
      />
    ));
  };

  render() {
    const { operators } = this.state;
    return <div>{this.renderCalculatorSummary(operators)}</div>;
  }
}

export default CalculatorComponentSummary;
