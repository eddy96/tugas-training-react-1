import React, { Component } from "react";
import CardSummaryContainer from "../components/pages/home/CardSummaryContainer";
import Calculator from "../components/pages/home/Calculator";
import "../components/pages/home/css/card-summary.css";

class Home extends Component {
  render() {
    return (
      <div>
        <h1>Dashboard</h1>
        <CardSummaryContainer />
        {/* <Calculator /> */}
      </div>
    );
  }
}
export default Home;
